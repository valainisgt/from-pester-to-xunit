function Invoke-PesterWithRetry {
[OutputType([Pester.Run])]
param (
    [Parameter(Mandatory = $true)]
    [Pester.ContainerInfo] $container,
    [Parameter(Mandatory = $true)]
    [int] $retryAttempts
)

$durations = [Pester.Run]::new()
$retryAttempt = 0
$result = $null

do {
    $result = Invoke-Pester -Container $container -PassThru -Output None

    $durations.DiscoveryDuration += $result.DiscoveryDuration
    $durations.Duration          += $result.Duration
    $durations.FrameworkDuration += $result.FrameworkDuration
    $durations.UserDuration      += $result.UserDuration
            
    $retryAttempt += 1
} while (($result.FailedCount -gt 0) -and ($retryAttempts -gt $retryAttempt))

$result.DiscoveryDuration = $durations.DiscoveryDuration
$result.Duration          = $durations.Duration
$result.FrameworkDuration = $durations.FrameworkDuration
$result.UserDuration      = $durations.UserDuration

return $result
}

Write-Host "Sequential Tests" -ForegroundColor DarkCyan
$containers = @(foreach($x in @(1..4)) {
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Create.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadAll.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadOne.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Update.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Delete.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Stop.Tests.ps1"
})

Measure-Command {
    foreach($container in $containers) {
        Invoke-PesterWithRetry -container $container -retryAttempts 5
    }
}

Write-Host "Parallel Tests" -ForegroundColor DarkCyan

$asyncContainers = @(foreach($x in @(1..4)) {
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Create.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadAll.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadOne.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Update.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Delete.Tests.ps1"
})

$sequentialContainers = @(foreach($x in @(1..5)) {
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Stop.Tests.ps1"
})

$funcDef = ${function:Invoke-PesterWithRetry}.ToString()
Measure-Command {
    $asyncContainers | ForEach-Object -Parallel {
        ${function:Invoke-PesterWithRetry} = $Using:funcDef
        Invoke-PesterWithRetry -container $_ -retryAttempts 5
    }

    $sequentialContainers | ForEach-Object {
        Invoke-PesterWithRetry -container $_ -retryAttempts 5
    }
}