$containers = @(
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Create.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadAll.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\ReadOne.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Update.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Delete.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Stop.Tests.ps1"
)

do {
    foreach($container in $containers) {
        & "$PSScriptRoot\..\Todo.PesterTests\Scripts\Invoke-PesterWithRetry.ps1" -container $container -retryAttempts 5 | Out-Null
    }
    Write-Host "Test Run Complete" -ForegroundColor DarkCyan
} while ($true)