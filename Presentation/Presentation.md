---
marp: true
---
![bg w:500 right](https://raw.githubusercontent.com/marp-team/marp/main/website/public/assets/marp-logo.svg)
# Flip the Script!
## From Pester to xUnit
by Greg Valainis

[https://gitlab.com/valainisgt](https://gitlab.com/valainisgt)

<!--
Hello everyone. My name is greg valainis.

Today we are going to be talking about testing.

The code for this presentation can be found at my gitlab profile

As I was developing this presentation I decided to cut the parts about provisioning and configuration labs using PowerShell.

That is a topic that deserves it own dedicated discussion.
-->

---

# Why do I like to test?

- Faster feedback cycles
- Confidence that my code is deployable and runs
- Forces you to write composable code
- It's a good idea &trade;

---
# Maintainable Tests
- Knowing how to write effective tests
- Diligent in removing test smells
- Smart usage of test patterns
![bg w:300 right:40%](http://xunitpatterns.com/Cover-Small.gif)

<!--
Writing a maintainable test suite can be difficult.

If you're like me, you thought since you knew how to program,
you would write tests. That could not be further from the truth.

I started many test suites, only to quickly give up because it was too much of a burden. I typically suffered from the fragile tests test smell.

It wasn't until I read the excellent book xunit test patterns,
that I truly began to see how to effectively write tests.

The book covers a plethora of test smells and test patterns.

Being able to recognize and act on these smells and patterns is half the battle.

Test Smells: Fragile Tests, Erratic Tests, Obscure Tests, Slow Tests

Test Patterns: Like the different fixture setup patterns, verification patterns, organization patterns.

We must must eliminate anything that is wrong,
anything that makes it unreliable,
and anything that makes it difficult to use.

If you let these things fester your test suite we become either ignored or useless. Neither of which is useful.
-->

---

# Background
- End to end test suite
- Windows enterprise software
- Extensive PowerShell usage

<!--
This presentation is really a case study/lessons learned from a project I was previously on.

In this project, I was tasked with building an end to end test suite. The test suite was there to provide confidence in the system and the new changes did not break existing functionality.

We needed to test against production like environments.

That means we were deploying into multi machine environments with a DC, DB, File servers, and more.

Being in a windows based environment we were making extensive use of powershell for the configuration of the environment and deployment of software.

Since we were already making extensive use of PowerShell,
Pester was a natural choice to build our test suite in.
-->

---

# Contrived Application
- Web API ToDo application
- Artificial delay in each request
- Requests will randomly fail
- Stop/Start ToDo related requests

<!--
In order to demonstrate what I learned from developing and extensive e2e test suite, I have put together a contrived application to help demonstrate.

This is a simple web api project some CRUD endpoints.

SHOW THE SWAGGER ENDPOINTS

SHOW THE TEST RESULTS
-->

---
# Pester

- "Ubiquitous test and mock framework for PowerShell"
- Behavior driven development style
- Similar to test frameworks in javascript (Mocha, Jasmine, Jest, etc)

![bg w:200 right:33%](https://raw.githubusercontent.com/pester/docs/master/static/img/logo_black.svg)

---
```ps1
Describe 'Description of SUT' {

    BeforeAll {
        # Do something before all tests
    }

    It 'Test Case 1' {
        # Test something
    }

    It 'Test Case 2' {
        # Test something
    }

    AfterEach {
        # Do something after each test case
    }
}
```
---
```ps1
Describe 'Description of SUT' {

    Describe 'More descriptive' {
        
        BeforeEach {
            # Will run before Test Case 1 and Test Case 2
        }
        
        It 'Test Case 1' {
            # Test something
        }

        Describe 'Even more descriptive' {

            It 'Test Case 2' {
                # Test something
            }
        }
    }
}
```
<!-- 
SHOW THE TEST SUITE

SHOW THE TEST RESULTS
-->
---

# Test Smell: Erratic Tests

- Sometimes they pass
- Sometimes they fail
- Tempted to stop running the tests
<!-- _footer: Meszaros, Gerard. (2007). xUnit Test Patterns: Refactoring Test Code, p. 228. -->

<!--
If you have ever developed a test suite with more 'realistic' usage of your application, you have probably encountered this.

The first step in removing erratic tests is to determine exactly why. This will probably take careful troubleshooting on your part.

If it turns out that this is caused by something within your control, obviously you need to take steps to rectify the issue.

Sometimes, erratic tests can occur based on something outside of your control. Using the file system is typically a point where I experience this issue.

In fact, I just experienced this the other day. You are using the file system from your software or the test suite itself and you have stated that you'd like to close the file.

However, the OS may hold that file handle open for much longer. You have no control over when it will close. This can cause issues when someone or something else attempts to perforn an action.

Other things that could be out of your control are dropped connections. Unreliable messaging patterns. Backdoor manipulation in general can succumb to this.

In your e2e test suites I'm sure you've run into this.

What are we to do to overcome this?

Well you can just run it again and hope for the best. But that isn't good enough.

One potential solution is to retry your test.

-->
---

# Retries in Pester

- Not supported by the framework
- Can be added without too much trouble
- Forces you to be more involved in the test enumeration and execution process

<!--
So we decided to implement retries in our test suite. You should be careful before you just start using retries because you could easily end up in a situation where you are getting false positives. But in the project this was based off of, we were confident that if we could converge on a green bar, the test had truly passed.

This change was not difficult, but it did force us to rethink some things.
-->

---
Invoke-PesterWithRetry.ps1
```ps1
param (
    [Pester.ContainerInfo] $container,
    [int]                  $retryAttempts = 5
)

$retryAttempt = 0
$result = $null

do {
    $result = Invoke-Pester -Container $container -PassThru
            
    $retryAttempt += 1

} while ($result.FailedCount -gt 0 `
         -and                      `
         $retryAttempts      -gt $retryAttempt)

return $result
```

<!--
Explain the code.

Explain how we had to our tests into separate tests,
which forced us to do the test enumeration.

SHOW THE TEST CODE BROKEN OUT

START RETRY TEST SUITE

START THE SLOW TEST SUITE

Great so we were able to fix our erratic test smell!
This change was straight fairly straight forward.

As you continue to add more and more tests, each test run will obviously take more and more time. In the project this presentation was based off of, our test suite was taking somewhere in the neighborhood of 12 hours. This isn't too bad, as you can start the test run at the end of the day and have results waiting for you in the morning. But we'd like to go faster. And if we continue to add tests, this is only going to get worse. We were suffering from the test smell: slow tests.

-->

---

# Test Smell: Slow Tests

- Tests take too long to run
- Reduced productivity
- Longer feedback cycle = less progress

<!-- _footer: Meszaros, Gerard. (2007). xUnit Test Patterns: Refactoring Test Code, p. 253. -->

<!--
An easy was to make your test suite faster is to run some tests in parallel.

Unfortunately that isn't possible in pester.

After we introduced our retry function we are now responsible for test enumeration and partly we have become the test runner.
PowerShell recently introduced a parallel for each so we can take advantage of that to introduce parallel execution of our tests
-->

---

## Parallel Pester Tests
```ps1
param(
    [Pester.ContainerInfo[]] $tests
)

$testResults = $tests | ForEach-Object -Parallel {
    Invoke-PesterWithRetry -Container $_ 
}
```
<!-- 
EXPLAIN THE CODE

This was very simple to implement. However, if we use this as is we have to potential to see the erratic tests smell again if we have certain tests that need to be alone. In my contrived application this could easily happen as we are testing an API endpoint that stops the processing of TODOs. During which a TODO test could be running and potentially fail. Now, obviously our retry would catch that and we should end up with a green bar, but this is wrong. We need to be able to specify which tests can be run in parallel and which ones must be run alone.
-->

---

## Controlling Execution

```ps1
param(
    [Pester.ContainerInfo[]] $parallelTests,
    [Pester.ContainerInfo[]] $sequentialTests
)

$testResults = $parallelTests | ForEach-Object -Parallel {
    Invoke-PesterWithRetry -Container $_ 
}

$testResults += $sequentialTests | ForEach-Object {
    Invoke-PesterWithRetry -Container $_ 
}

```
<!-- 
All we have done is modified how the test enumeration is handled.

Ok so we have over come several test smells without too much trouble.

However, I havent' been incredibly pleased with the results. I can already see more complex fixture setups on the horizon and accomplishing what I want is going to be unnatural for powershell.

What I really think I need is a test framework that has capabilities and points of extension for me to use to accomplish the hurdles we have encountered.
-->
---

# xUnit Style Test Framework

- Specify a test case as a test method
- Specify the expected results as assertions
- Aggregate tests into test suites
- Run one or more tests and get a report

---

# Modern .NET Test Frameworks
- xUnit&period;net, MSTest, NUnit
- Provide control over test execution
- Can be extended for many different purposes

---
# Test Retries in xUnit
- Included in NUnit
- Sample shown in xUnit&period;net repository
- Simple in MSTest
---
## Test Retry (MSTest)
```cs
public class RetryTestMethodAttribute : TestMethodAttribute {
    private int retryCount = 5;
    public override TestResult[] Execute(ITestMethod testMethod)
    {
        int retryAttempt = 0;
        var results = Array.Empty<TestResult>();

        do {

            results = base.Execute(testMethod);
            retryAttempt++;

        } while (
            retryAttempt < retryCount && 
            results.Any(x => x.Outcome == UnitTestOutcome.Failed));

        return results;
    }
}
```
---
Using Retry (MSTest)
```cs
public class TestClass1
{
    [TestMethod]
    public void Test1()
    {
        Thread.Sleep(3000);
    }

    [RetryTestMethod]
    public void Test2()
    {
        Thread.Sleep(5000);
    }
}
```
---
# Controlling Test Execution


- Typically, tests within the same collection are run sequentially
- Typically, different test collections are run in parallel
- Collections can be customized

---

## Sequential Execution (xUnit&period;net)
```cs
public class TestClass1
{
    [Fact]
    public void Test1()
    {
        Thread.Sleep(3000);
    }

    [Fact]
    public void Test2()
    {
        Thread.Sleep(5000);
    }
}
```

---

## Parallel Execution (xUnit&period;net)
```cs
public class TestClass1
{
    [Fact]
    public void Test1()
    {
        Thread.Sleep(3000);
    }
}

public class TestClass2
{
    [Fact]
    public void Test2()
    {
        Thread.Sleep(5000);
    }
}
```
---
## Named Collections (xUnit&period;net)
```cs
[CollectionDefinition("TestCollection1")]
public class TestCollection1 { }

[Collection("TestCollection1")]
public class TestClass1
{
    [Fact]
    public void Test1()
    ...
}

[Collection("TestCollection1")]
public class TestClass2
{
    [Fact]
    public void Test2()
    ...
}
```
---
# Fine Grained Control of Execution
```cs
// xUnit.net 
[CollectionDefinition("Non-Parallel Collection", DisableParallelization = true)]

// MSTest
[Parallelize(Workers = 3, Scope = ExecutionScope.MethodLevel)]

[DoNotParallelize]

// NUnit
[Parallelizable(ParallelScope.Self)]

[NonParallelizable]

```
<!--
EXPLAIN THE CODE

We have seen in multiple xUnit frameworks how the things we wanted out of Pester just come naturally. 
-->
---
# What have we missed?
- Consequences of PowerShell removal - Microsoft.PowerShell.SDK
- Configuring machines
- Provisioning machines
---
# In Review
- Pester is a great tool!
- Introduced extended functionality in Pester
- Went from Pester to xUnit
