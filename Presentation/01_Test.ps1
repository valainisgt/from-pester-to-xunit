$failureOccurred = $false
$attempt = 0
$continue = $true
do {
    $result = Invoke-Pester -Path "$PSScriptRoot\..\Todo.PesterTests\TestScripts\Original.Tests.ps1" -PassThru
    $failureOccurred = $failureOccurred -or ($result.FailedCount -gt 0)
    $attempt += 1

    Write-Host "Test Run Complete" -ForegroundColor DarkCyan

    if ($attempt -gt 4) {
        $continue = (-not $failureOccurred)
    }
} while ($continue)