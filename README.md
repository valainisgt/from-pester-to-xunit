# From Pester to xUnit
This repository houses the companion code for my presentation, 'From Pester to xUnit'.
In this presentation I walk through several test refactorings to handle test smells like 
[erratic tests](http://xunitpatterns.com/Erratic%20Test.html) and 
[slow tests](http://xunitpatterns.com/Slow%20Tests.html) in Pester.
After several attempts to overcome some of Pester's limitations,
I move the test suite to an xUnit style test framework where my previous workarounds for Pester are provided out of the box.

The slides for the presentation can be found in artifacts for the 'PowerPoint' job of the most recent CI/CD pipeline.
