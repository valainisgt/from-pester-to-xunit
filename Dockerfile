FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app

COPY publish/Todo.WebApi ./

EXPOSE 80

ENTRYPOINT ["dotnet", "Todo.WebApi.dll"]
