param(
    [Parameter()]
    [ValidateNotNull()]
    [string] $adminStopUrl = 'http://localhost:5000/admin/stop',

    [Parameter()]
    [ValidateNotNull()]
    [string] $adminStartUrl = 'http://localhost:5000/admin/start'
)

Describe 'Admin Controller' {
    It 'Can Stop' {
        $restParameters = @{
            Method             = 'Post'
            Uri                = $adminStopUrl
            ContentType        = 'application/json'
            SkipHttpErrorCheck = $true
            StatusCodeVariable = 'scv'
        }
        Invoke-RestMethod @restParameters
        $scv | Should -Be 202
    }
    AfterEach {
        $restParameters = @{
            Method      = 'Post'
            Uri         = $adminStartUrl
            ContentType = 'application/json'
        }
        Invoke-RestMethod @restParameters
    }
}
