param(
    [Parameter()]
    [ValidateNotNull()]
    [string] $todoUrl = 'http://localhost:5000/todos'
)

Describe 'Todos Controller' {
    It 'Can Read' {
        $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"
        $restParameters = @{
            Method      = 'Post'
            Uri         = $todoUrl
            Body        = ConvertTo-Json $todo
            ContentType = 'application/json'
        }
        Invoke-RestMethod @restParameters

        $restParameters = @{
            Method             = 'Get'
            Uri                = "$todoUrl/$($todo.Id)"
            SkipHttpErrorCheck = $true
            StatusCodeVariable = 'scv'
        }
        Invoke-RestMethod @restParameters

        $scv | Should -Be 200
    }
}
