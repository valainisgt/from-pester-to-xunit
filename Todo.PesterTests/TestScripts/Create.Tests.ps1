param(
    [Parameter()]
    [ValidateNotNull()]
    [string] $todoUrl = 'http://localhost:5000/todos'
)
Describe 'Todos Controller' {
    It 'Can Create' {
        $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"

        $restParameters = @{
            Method             = 'Post'
            Uri                = $todoUrl
            Body               = ConvertTo-Json $todo
            ContentType        = 'application/json'
            SkipHttpErrorCheck = $true
            StatusCodeVariable = 'scv'
        }
        Invoke-RestMethod @restParameters

        $scv | Should -Be 201
    }
}
