param(
    [Parameter()]
    [ValidateNotNull()]
    [string] $todoUrl = 'http://localhost:5000/todos',

    [Parameter()]
    [ValidateNotNull()]
    [string] $adminStopUrl = 'http://localhost:5000/admin/stop',

    [Parameter()]
    [ValidateNotNull()]
    [string] $adminStartUrl = 'http://localhost:5000/admin/start'
)

Describe 'Todo WebApi' {
    Describe 'Todos' {
        It 'Can Create' {
            $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"

            $restParameters = @{
                Method             = 'Post'
                Uri                = $todoUrl
                Body               = ConvertTo-Json $todo
                ContentType        = 'application/json'
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters

            $scv | Should -Be 201
        }
        It 'Can Read All' {

            $restParameters = @{
                Method             = 'Get'
                Uri                = $todoUrl
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters

            $scv | Should -Be 200
        }
        It 'Can Read One' {
            $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"
            $restParameters = @{
                Method      = 'Post'
                Uri         = $todoUrl
                Body        = ConvertTo-Json $todo
                ContentType = 'application/json'
            }
            Invoke-RestMethod @restParameters

            $restParameters = @{
                Method             = 'Get'
                Uri                = "$todoUrl/$($todo.Id)"
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters

            $scv | Should -Be 200
        }
        It 'Can Update' {
            $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"
            $restParameters = @{
                Method      = 'Post'
                Uri         = $todoUrl
                Body        = ConvertTo-Json $todo
                ContentType = 'application/json'
            }
            Invoke-RestMethod @restParameters
    
            $todo.IsComplete = $false
            $restParameters = @{
                Method             = 'Put'
                Uri                = "$todoUrl/$($todo.Id)"
                Body               = ConvertTo-Json $todo
                ContentType        = 'application/json'
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters

            $scv | Should -Be 204
        }
        It 'Can Delete' {
            $todo =  & "$PSScriptRoot\..\Scripts\New-Todo.ps1"
            $restParameters = @{
                Method      = 'Post'
                Uri         = $todoUrl
                Body        = ConvertTo-Json $todo
                ContentType = 'application/json'
            }
            Invoke-RestMethod @restParameters

            $restParameters = @{
                Method             = 'Delete'
                Uri                = "$todoUrl/$($todo.Id)"
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters

            $scv | Should -Be 204
        }
    }
    Describe 'Admin' {
        It 'Can Stop' {
            $restParameters = @{
                Method             = 'Post'
                Uri                = $adminStopUrl
                ContentType        = 'application/json'
                SkipHttpErrorCheck = $true
                StatusCodeVariable = 'scv'
            }
            Invoke-RestMethod @restParameters
            $scv | Should -Be 202
        }
        AfterEach {
            $restParameters = @{
                Method      = 'Post'
                Uri         = $adminStartUrl
                ContentType = 'application/json'
            }
            Invoke-RestMethod @restParameters
        }
    }
}