param(
    [Parameter()]
    [ValidateNotNull()]
    [string] $todoUrl = 'http://localhost:5000/todos'
)

Describe 'Todos Controller' {
    It 'Can Read All' {
        $restParameters = @{
            Method             = 'Get'
            Uri                = $todoUrl
            SkipHttpErrorCheck = $true
            StatusCodeVariable = 'scv'
        }
        Invoke-RestMethod @restParameters

        $scv | Should -Be 200
    }
}
