$asyncContainers = @(
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\Create.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\ReadAll.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\ReadOne.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\Update.Tests.ps1"
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\Delete.Tests.ps1"
)

$sequentialContainers = @(
    New-PesterContainer -Path "$PSScriptRoot\TestScripts\Stop.Tests.ps1"
)

$testResults = $asyncContainers | ForEach-Object -Parallel {
    $result = & "$Using:PSScriptRoot\Scripts\Invoke-PesterWithRetry.ps1" -container $_ -retryAttempts 5
    $result
}

$testResults += @(foreach($container in $sequentialContainers) {
    $result = & "$PSScriptRoot\Scripts\Invoke-PesterWithRetry.ps1" -container $container -retryAttempts 5
    $result
})

& "$PSScriptRoot\Scripts\Assert-Results.ps1" -testResults $testResults
