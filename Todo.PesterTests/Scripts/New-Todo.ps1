param()

$id = [Guid]::NewGuid()
@{
    Id = $id
    Text = "Todo $id"
    Completed = $false
}