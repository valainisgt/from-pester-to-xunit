param(
    [Parameter(Mandatory = $true)]
    [Pester.Run[]] $testResults
)

$pester = [Pester.Run]::new()
foreach ($result in $testResults) {
    $pester.Containers            += $result.Containers
    $pester.DiscoveryDuration     += $result.DiscoveryDuration
    $pester.Duration              += $result.Duration
    $pester.Executed              += $result.Executed
    $pester.Failed                += $result.Failed
    $pester.FailedBlocks          += $result.FailedBlocks
    $pester.FailedBlocksCount     += $result.FailedBlocksCount
    $pester.FailedContainers      += $result.FailedContainers
    $pester.FailedContainersCount += $result.FailedContainersCount
    $pester.FailedCount           += $result.FailedCount
    $pester.FrameworkDuration     += $result.FrameworkDuration
    $pester.NotRun                += $result.NotRun
    $pester.NotRunCount           += $result.NotRunCount
    $pester.Passed                += $result.Passed
    $pester.PassedCount           += $result.PassedCount
    $pester.Skipped               += $result.Skipped
    $pester.SkippedCount          += $result.SkippedCount
    $pester.Tests                 += $result.Tests
    $pester.TotalCount            += $result.TotalCount
    $pester.UserDuration          += $result.UserDuration
}

$pester.ExecutedAt = Get-Date
$pester.Result = $pester.FailedCount -eq 0 ? 'Passed' : 'Failed'

Write-Host ($pester | Out-String)

if($pester.FailedCount -gt 0) {
    throw 'Tests Failed!'
}