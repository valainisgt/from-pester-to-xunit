[OutputType([Pester.Run])]
param (
    [Parameter(Mandatory = $true)]
    [Pester.ContainerInfo] $container,
    [Parameter(Mandatory = $true)]
    [int] $retryAttempts
)

$durations = [Pester.Run]::new()
$retryAttempt = 0
$result = $null

do {
    $result = Invoke-Pester -Container $container -PassThru

    $durations.DiscoveryDuration += $result.DiscoveryDuration
    $durations.Duration          += $result.Duration
    $durations.FrameworkDuration += $result.FrameworkDuration
    $durations.UserDuration      += $result.UserDuration
            
    $retryAttempt += 1
} while (($result.FailedCount -gt 0) -and ($retryAttempts -gt $retryAttempt))

$result.DiscoveryDuration = $durations.DiscoveryDuration
$result.Duration          = $durations.Duration
$result.FrameworkDuration = $durations.FrameworkDuration
$result.UserDuration      = $durations.UserDuration

return $result