using System.ComponentModel;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Todo.XunitTests;

[Serializable]
public class RetryTestCase : XunitTestCase {
    private int maxRetries;

    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Called by the de-serializer; should only be called by deriving classes for de-serialization purposes")]
    public RetryTestCase() { }

    public RetryTestCase(
        IMessageSink diagnosticMessageSink,
        TestMethodDisplay testMethodDisplay,
        TestMethodDisplayOptions defaultMethodDisplayOptions,
        ITestMethod testMethod,
        int maxRetries)
            : base(
                diagnosticMessageSink,
                testMethodDisplay,
                defaultMethodDisplayOptions,
                testMethod,
                testMethodArguments: null)
        {    
            this.maxRetries = maxRetries;
        }

    public override async Task<RunSummary> RunAsync(IMessageSink diagnosticMessageSink,
                                                    IMessageBus messageBus,
                                                    object[] constructorArguments,
                                                    ExceptionAggregator aggregator,
                                                    CancellationTokenSource cancellationTokenSource) {
        var runCount = 0;

        while (true) {
            var delayedMessageBus = new DelayedMessageBus(messageBus);

            var summary = await base.RunAsync(diagnosticMessageSink, delayedMessageBus, constructorArguments, aggregator, cancellationTokenSource);
            if (aggregator.HasExceptions || summary.Failed == 0 || ++runCount >= maxRetries) {
                delayedMessageBus.Dispose();
                return summary;
            }

            diagnosticMessageSink.OnMessage(new DiagnosticMessage("Execution of '{0}' failed (attempt #{1}), retrying...", DisplayName, runCount));
        }
    }

    public override void Serialize(IXunitSerializationInfo data) {
        base.Serialize(data);

        data.AddValue("MaxRetries", maxRetries);
    }

    public override void Deserialize(IXunitSerializationInfo data) {
        base.Deserialize(data);

        maxRetries = data.GetValue<int>("MaxRetries");
    }
}

public class RetryFactDiscoverer : IXunitTestCaseDiscoverer {
    readonly IMessageSink diagnosticMessageSink;

    public RetryFactDiscoverer(IMessageSink? diagnosticMessageSink) {
        this.diagnosticMessageSink = diagnosticMessageSink ?? throw new ArgumentNullException(nameof(diagnosticMessageSink));
    }

    public IEnumerable<IXunitTestCase> Discover(ITestFrameworkDiscoveryOptions discoveryOptions, ITestMethod testMethod, IAttributeInfo factAttribute) {
        var maxRetries = factAttribute.GetNamedArgument<int>("MaxRetries");
        
        yield return new RetryTestCase(
            diagnosticMessageSink,
            discoveryOptions.MethodDisplayOrDefault(),
            discoveryOptions.MethodDisplayOptionsOrDefault(),
            testMethod,
            maxRetries);
    }
}

[XunitTestCaseDiscoverer("Todo.XunitTests.RetryFactDiscoverer", "Todo.XunitTests")]
public class RetryFactAttribute : FactAttribute {
    private int maxRetries = 1;
    public int MaxRetries { 
        get => this.maxRetries;
        set => this.maxRetries = value > 0 ? value : 1;
    }
}

public class DelayedMessageBus : IMessageBus {
    private readonly IMessageBus innerBus;
    private readonly List<IMessageSinkMessage> messages = new List<IMessageSinkMessage>();

    public DelayedMessageBus(IMessageBus innerBus) {
        this.innerBus = innerBus;
    }

    public bool QueueMessage(IMessageSinkMessage message) {
        lock (messages) {
            messages.Add(message);
        }
        return true;
    }

    public void Dispose() {
        foreach (var message in messages) {
            innerBus.QueueMessage(message);
        }        
    }
}
