using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace Todo.XunitTests;

public record TodoDto (Guid Id, string Text, bool IsComplete);

public static class Todos {
    public static TodoDto NewTodo() => new TodoDto(Guid.NewGuid(), "Hello World", false);
    private static readonly Lazy<string> TodoAddress = new Lazy<string>(() =>
        new ConfigurationBuilder()
            .AddInMemoryCollection(new Dictionary<string, string>() {
                ["Todos:BaseAddress"] = "http://localhost:5000"
            })
            .AddEnvironmentVariables()
            .Build()
            .GetSection("todos")
            ["baseaddress"]);
    public static readonly HttpClient HttpClient = new HttpClient() {
        BaseAddress = new Uri(TodoAddress.Value)
    };
    public static void AssertSuccess(this HttpResponseMessage response) {
        Xunit.Assert.True(
            response.IsSuccessStatusCode,
            $"Actual status code: {response.StatusCode}.");
    }
}
