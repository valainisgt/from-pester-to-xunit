using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Todo.XunitTests;

public class CreateTodoTests {

    [RetryFact(MaxRetries = 5)]
    public async Task CanCreate() {

        var resp = await Todos.HttpClient.PostAsJsonAsync<TodoDto>(
            "todos",
            Todos.NewTodo()
        );

        resp.AssertSuccess();
    }
}

public class ReadAllTodoTests {

    [RetryFact(MaxRetries = 5)]
    public async Task CanRead() {

        var resp = await Todos.HttpClient.GetAsync("todos");

        resp.AssertSuccess();
    }
}

public class ReadOneTodoTests {

    [RetryFact(MaxRetries = 5)]
    public async Task CanRead() {

        var todo = Todos.NewTodo();
        var _ = await Todos.HttpClient.PostAsJsonAsync<TodoDto>(
            "todos",
            todo
        );

        todo = todo with { IsComplete = true };
        var resp = await Todos.HttpClient.GetAsync(
            $"todos/{todo.Id}"
        );

        resp.AssertSuccess();
    }
}

public class UpdateTodoTests {

    [RetryFact(MaxRetries = 5)]
    public async Task CanUpdate() {

        var todo = Todos.NewTodo();
        var _ = await Todos.HttpClient.PostAsJsonAsync<TodoDto>(
            "todos",
            todo
        );

        todo = todo with { IsComplete = true };
        var resp = await Todos.HttpClient.PutAsJsonAsync<TodoDto>(
            $"todos/{todo.Id}",
            todo
        );

        resp.AssertSuccess();
    }
}

public class DeleteTodoTests {

    [RetryFact(MaxRetries = 5)]
    public async Task CanDelete() {

        var todo = Todos.NewTodo();
        var _ = await Todos.HttpClient.PostAsJsonAsync<TodoDto>(
            "todos",
            todo
        );

        todo = todo with { IsComplete = true };
        var resp = await Todos.HttpClient.DeleteAsync(
            $"todos/{todo.Id}"
        );

        resp.AssertSuccess();
    }
}
