using System.Threading.Tasks;
using Xunit;

namespace Todo.XunitTests;

[CollectionDefinition("Non-Parallel Collection", DisableParallelization = true)]
public class NonParallelCollectionDefinitionClass { }

[Collection("Non-Parallel Collection")]
public class StopTests : IAsyncLifetime {

    [Fact]
    public async Task CanStop() {

        var resp = await Todos.HttpClient.PostAsync(
            "admin/stop",
            null
        );

        resp.AssertSuccess();
    }
    
    public Task InitializeAsync() { return Task.CompletedTask; }
    public async Task DisposeAsync() {
        var _ = await Todos.HttpClient.PostAsync("admin/start", null);
    }
}
