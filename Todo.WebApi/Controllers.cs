using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Todo.WebApi;

[Route("[controller]")]
public class TodosController : ControllerBase {
    private readonly ICrud<Todo> crud;
    public TodosController(ICrud<Todo>? crud) {
        this.crud = crud ?? throw new ArgumentNullException(nameof(crud));
    }
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Todo>>> GetAll(CancellationToken ct) {
        var results = await this.crud.ReadAsync(ct);
        return Ok(results);
    }
    [HttpGet("{id}")]
    public async Task<IActionResult> Get(Guid id, CancellationToken ct) {
        var todos = await this.crud.ReadAsync(ct);
        var todo = todos.FirstOrDefault(x => x.Id == id);
        if(todo is null) {
            return NotFound();
        }
        else {
            return Ok(todo);
        }
    }
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] Todo todo, CancellationToken ct) {
        await this.crud.CreateAsync(todo, ct);
        return StatusCode(201);
    }
    [HttpPut("{id}")]
    public async Task<IActionResult> Put(Guid id, [FromBody] Todo todo, CancellationToken ct) {
        todo.Id = id;
        await this.crud.UpdateAsync(todo, ct);
        return NoContent();
    }
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(Guid id, CancellationToken ct) {
        await this.crud.DeleteAsync(new Todo() { Id = id }, ct);
        return NoContent();
    }
}

[Route("[controller]")]
public class AdminController : ControllerBase {
    private readonly GateKeeperCrud.Gate gate;
    public AdminController(GateKeeperCrud.Gate gate) {
        this.gate = gate ?? throw new ArgumentNullException(nameof(gate));
    }
    [HttpPost]
    [Route(nameof(Stop))]
    public IActionResult Stop() {
        this.gate.Close();
        return Accepted();
    }

    [HttpPost]
    [Route(nameof(Start))]
    public IActionResult Start() {
        this.gate.Open();
        return Accepted();
    }
}
