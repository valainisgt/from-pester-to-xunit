using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Todo.WebApi;

[assembly: ApiController]

// The in-memory database only persists while a connection is open to it.
const string connectionString = "Data Source=Todos;Mode=Memory;Cache=Shared";
var masterConnection = new SqliteConnection(connectionString);
masterConnection.Open();

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<TodosContext>(opt => {
    opt.UseSqlite(connectionString);
});

builder.Services.AddSingleton<Random>();

builder.Services.AddSingleton<GateKeeperCrud.Gate>(_ => {
    var gate = new GateKeeperCrud.Gate();
    gate.Open();
    return gate;
});

builder.Services.AddScoped<ICrud<Todo.WebApi.Todo>>(serviceProvider =>
    new GateKeeperCrud(
        serviceProvider.GetRequiredService<GateKeeperCrud.Gate>(),
        new DelayCrud(
            new RandomFailureCrud(
                new DatabaseCrud(
                    serviceProvider.GetRequiredService<TodosContext>()),
                serviceProvider.GetRequiredService<Random>(),
                20),
            serviceProvider.GetRequiredService<Random>())));

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

// Create the database
var db = app.Services.CreateScope().ServiceProvider.GetRequiredService<TodosContext>();
await db.Database.EnsureCreatedAsync();

app.Run();
