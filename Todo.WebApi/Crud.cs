using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Todo.WebApi;

public interface ICrud<T> {
    public Task CreateAsync(T x, CancellationToken ct);
    public Task<IList<T>> ReadAsync(CancellationToken ct);
    public Task UpdateAsync(T x, CancellationToken ct);
    public Task DeleteAsync(T x, CancellationToken ct);
}

public class DatabaseCrud : ICrud<Todo> {
    private readonly TodosContext db;
    public DatabaseCrud(TodosContext? db) {
        this.db = db ?? throw new ArgumentNullException(nameof(db));
    }
    public async Task CreateAsync(Todo x, CancellationToken ct) {
        this.db.Add(x);
        await this.db.SaveChangesAsync(ct);
    }
    public async Task<IList<Todo>> ReadAsync(CancellationToken ct) {
        var results = await this.db.Todos.ToListAsync();
        return results;
    }
    public async Task UpdateAsync(Todo x, CancellationToken ct) {
        this.db.Update(x);
        await this.db.SaveChangesAsync(ct);
    }
    public async Task DeleteAsync(Todo x, CancellationToken ct) {
        this.db.Entry(x).State = EntityState.Deleted;
        await this.db.SaveChangesAsync(ct);
    }
}

public class DelayCrud : ICrud<Todo> {
    private readonly ICrud<Todo> origin;
    private readonly Random random;
    public DelayCrud(ICrud<Todo>? origin, Random random) {
        this.origin = origin ?? throw new ArgumentNullException(nameof(origin));
        this.random = random ?? throw new ArgumentNullException(nameof(random));
    }
    public async Task CreateAsync(Todo x, CancellationToken ct) {
        await this.createDelay(ct);
        await this.origin.CreateAsync(x, ct);
    }
    public async Task<IList<Todo>> ReadAsync(CancellationToken ct) {
        await this.createDelay(ct);
        return await this.origin.ReadAsync(ct);
    }
    public async Task UpdateAsync(Todo x, CancellationToken ct) {
        await this.createDelay(ct);
        await this.origin.UpdateAsync(x, ct);
    }
    public async Task DeleteAsync(Todo x, CancellationToken ct) {
        await this.createDelay(ct);
        await this.origin.DeleteAsync(x, ct);
    }
    private Task createDelay(CancellationToken ct) => Task.Delay(this.random.Next(700, 800), ct);
}

public class RandomFailureCrud : ICrud<Todo> {
    private readonly Random random;
    private readonly ICrud<Todo> origin;
    private readonly int oneInXChance;
    public RandomFailureCrud(ICrud<Todo>? origin, Random random, int oneInXChance) {
        this.origin = origin ?? throw new ArgumentNullException(nameof(origin));
        this.random = random ?? throw new ArgumentNullException(nameof(random));
        if(oneInXChance < 1) { throw new ArgumentOutOfRangeException(nameof(oneInXChance), "Must be at least one."); }
        this.oneInXChance = oneInXChance;
    }
    class SimulatedException : Exception { }
    private void randomFailure() {
        if (random.Next(0, this.oneInXChance) == 0) { throw new SimulatedException(); }
    }
    public async Task CreateAsync(Todo x, CancellationToken ct) {
        this.randomFailure();
        await this.origin.CreateAsync(x, ct);
    }
    public async Task<IList<Todo>> ReadAsync(CancellationToken ct) {
        this.randomFailure();
        return await this.origin.ReadAsync(ct);
    }
    public async Task UpdateAsync(Todo x, CancellationToken ct) {
        this.randomFailure();
        await this.origin.UpdateAsync(x, ct);
    }
    public async Task DeleteAsync(Todo x, CancellationToken ct) {
        this.randomFailure();
        await this.origin.DeleteAsync(x, ct);
    }
}

public class GateKeeperCrud : ICrud<Todo> {
    private readonly Gate gate;
    private readonly ICrud<Todo> origin;
    public GateKeeperCrud(Gate? gate, ICrud<Todo>? origin) {
        this.origin = origin ?? throw new ArgumentNullException(nameof(origin));
        this.gate = gate ?? throw new ArgumentNullException(nameof(gate));
    }
    public async Task CreateAsync(Todo x, CancellationToken ct) {
        this.passThroughGate();
        await this.origin.CreateAsync(x, ct);
    }
    public async Task<IList<Todo>> ReadAsync(CancellationToken ct) {
        this.passThroughGate();
        return await this.origin.ReadAsync(ct);
    }
    public async Task UpdateAsync(Todo x, CancellationToken ct) {
        this.passThroughGate();
        await this.origin.UpdateAsync(x, ct);
    }
    public async Task DeleteAsync(Todo x, CancellationToken ct) {
        this.passThroughGate();
        await this.origin.DeleteAsync(x, ct);
    }
    private void passThroughGate() {
        if (!this.gate.IsOpen()) {
            throw new GateClosedException();
        }
    }
    class GateClosedException : Exception { }
    public class Gate {
        private bool isOpen = false;
        private readonly object gateLock = new();
        public void Open() {
            lock (gateLock) {
                this.isOpen = true;
            }
        }
        public void Close() {
            lock (gateLock) {
                this.isOpen = false;
            }
        }
        public bool IsOpen() {
            lock (gateLock) {
                return this.isOpen;
            }
        }
    }
}