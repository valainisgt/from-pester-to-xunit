using System;
using Microsoft.EntityFrameworkCore;

namespace Todo.WebApi;

public class Todo {
    public Guid Id { get; set; } = Guid.Empty;
    public string Text { get; set; } = "";
    public bool IsComplete { get; set; } = false;
}

public class TodosContext : DbContext {
    public TodosContext(DbContextOptions<TodosContext> options) : base(options) { }
    public DbSet<Todo> Todos { get; set; } = null!;
}
